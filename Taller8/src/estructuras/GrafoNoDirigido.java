package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends Nodo> implements Grafo<T> {
	
	private int numNodos;
	private int numArcos;

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private Nodo[] nodos;

	/**
	 * Lista de adyacencia 
	 */
	private HashMap<Integer, List<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado.
	private Arco[] arcos;

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		nodos = new Nodo[493];
		numNodos = 0;
		numArcos = 0;
	}

	@Override
	public boolean agregarNodo(T nodo) {
		//TODO implementar
		try{
		nodos[numNodos] = nodo;
		return true;
		}
		catch(Exception e){
			return false;
		}
	}

	@Override
	public boolean eliminarNodo(int id) {
		boolean encontrado = false;
		for(int i = 0; i < nodos.length ; i++){
			if(nodos[i].darId() == id){
				nodos[i] = null;
				encontrado = true;
			}
		}
		if(encontrado == true){
			int nuevoNumNodos = numNodos - 1;
			resizeNodos(nuevoNumNodos);
		}
		return encontrado;
		
	}

	private void resizeNodos(int pNumNodos) {
		// TODO Auto-generated method stub
		Nodo[] nuevoArreglo = new Nodo[pNumNodos];
		int j = 0;
		for(int i = 0; i < nodos.length; i++){
			if(nodos[i] != null){
				nuevoArreglo[j] = nodos[i];
				j++;
			}
		}
		nodos = nuevoArreglo;
		numNodos = pNumNodos;
		
	}

	@Override
	public Arco[] darArcos() {
		//TODO implementar
		return arcos;
	}

	private <E extends Comparable<E>> Arco crearArco( final int inicio, final int fin, final double costo, E e )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e);
	}

	@Override
	public Nodo[] darNodos() {
		//TODO implementar
		return nodos;
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj) {
		//TODO implementar
		Arco nuevoArco = crearArco(i,f,costo,obj);
		
	}

	@Override
	public boolean agregarArco(int i, int f, double costo) {
		return agregarArco(i, f, costo, null);
	}

	@Override
	public Arco eliminarArco(int inicio, int fin) {
		//TODO implementar
	}

	@Override
	public Nodo buscarNodo(int id) {
		//TODO implementar
	}

	@Override
	public Arco[] darArcosOrigen(int id) {
		//TODO implementar
	}

	@Override
	public Arco[] darArcosDestino(int id) {
		//TODO implementar
	}

}
