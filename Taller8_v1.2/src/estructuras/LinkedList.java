package estructuras;


/**
 * Para realizar esta clase, se utilizó como referencia el paquete de estructuras lineales 
 * enviado por el profesor.
 * Es código open source con licencia MIT.
 * @param <T>
 */
public class LinkedList<K extends Comparable<K> ,V> {

	private NodoHash first;
	private int size;
	
	public LinkedList(){
		first = null;
		size = 0;
	    
	}
	
	
	public NodoHash getFirst() {
		return first;
	}

	public int getSize(){
		return size;
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	
	public void add(K llave, V valor){
		NodoHash current = first;
		NodoHash newNode = new NodoHash(llave, valor);
		
		
		if(first == null){
			first = newNode;
			size++;
			return;
		}
		
		while (current.getNext() != null){
			current =current.getNext();
		}
		current.setNext(newNode);
		size++;
	}
	
	
	
	
	public void deleteAtK(int k){
		NodoHash previous = null;

		if(k == 0){	    
	    	first = first.getNext();
	    	size--;
	    	return;
	    }
		
		int i = 0;
		NodoHash current = first;
		
		while (i < k && current != null){
		
			previous = current;
			current =current.getNext();
			
			i++;
		}
		previous.setNext(current.getNext());
		size--;
		
	}
	
	
	public V getValor(int k)
	{
		NodoHash current = first;
		V objeto = null;
		boolean retornado = false;
		
		int i = 0;
		while(i < size && !retornado)
		{
			if(i == k)
			{
				objeto = (V) current.getValor();
				retornado = true;
			}
			else
			{
				current = current.getNext();
			}
			
			i++;
		}
		
		return objeto;
	}
	
	public K getLlave(int k)
	{
		NodoHash current = first;
		K objeto = null;
		boolean retornado = false;
		
		int i = 0;
		while(i < size && !retornado)
		{
			if(i == k)
			{
				objeto = (K) current.getLlave();
				retornado = true;
			}
			else
			{
				current = current.getNext();
			}
			
			i++;
		}
		
		return objeto;
	}

	


}