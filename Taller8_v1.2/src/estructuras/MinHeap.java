package estructuras;

public class MinHeap<Key extends Comparable<Key>> 
{

	private Key[] pq;
	private int cantidadDentro = 0;
	private int capacidad = 0;

	public MinHeap(int pCantidadDentro)
	{
		//pq = (Key[]) Array.newInstance(pedido, pCantidadDentro +1);
		pq = (Key[]) new Comparable[pCantidadDentro +1];
		capacidad = pCantidadDentro;
	}


	public void add(Key elemento) 
	{
		// TODO Auto-generated method stub
		if(cantidadDentro < capacidad)
		{
			pq[++cantidadDentro] = elemento;
			siftUp();

		}
		else
		{

	        Key[] temp = (Key[]) new Comparable[2*pq.length];
	        for (int i = 1; i <= cantidadDentro; i++) {
	            temp[i] = pq[i];
	        }
	        pq = temp;
			
			pq[++cantidadDentro] = elemento;
			siftUp();
			
		}


	}


	public Key peek() 
	{
		// TODO Auto-generated method stub
		if(isEmpty())
		{
			return null;
		}
		else
		{
			Key max = pq[1];
			return max;
		}
	}


	public Key poll() 
	{
		// TODO Auto-generated method stub
		if(!isEmpty())
		{
			Key max = pq[1];
			exchange(1, cantidadDentro--);
			pq[cantidadDentro+1] = null;
			siftDown();
			return max;
		}
		else
		{
			return null;
		}

		
	}


	public int size() 
	{
		// TODO Auto-generated method stub
		return cantidadDentro;
	}


	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return cantidadDentro == 0;
	}


	public void siftUp() 
	{
		int k = cantidadDentro;
		while(k > 1 && !less(k/2, k))
		{
			exchange(k/2, k);
			k = k/2;
		}
		// TODO Auto-generated method stub

	}


	public void siftDown() 
	{
		// TODO Auto-generated method stub
		int k = 1;
		while (2*k <= cantidadDentro)
		{
			int j = 2*k;
			if (j < cantidadDentro && less(j, j+1)) j++;
			if (less(k, j)) break;
			exchange(k, j);
			k = j;

		}
	}


	public Key[] darLista()
	{

		return pq;
	}

	public void exchange(int p, int k)
	{
		Key temp = pq[p];
		pq[p] = pq[k];
		pq[k] = temp;
	}


	public boolean less(int padre, int hijo)
	{
		return pq[padre].compareTo(pq[hijo]) < 0;
	}



}
