package estructuras;


/**
 * Para realizar esta clase, se utilizó como referencia el paquete de estructuras lineales 
 * enviado por el profesor.
 * Es código open source
 * @param <T>
 */
public class Node <T>{

	private T item;
	private Node<T> next;
	

	public Node() {
		
		this.item = null;
		this.next = null;
	}
	
	
	public Node(T item, Node<T> next) {
		super();
		this.item = item;
		this.next = next;
	}
	
	
	
	public T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
	}
	public Node<T> getNext() {
		return next;
	}
	public void setNext(Node<T> next) {
		this.next = next;
	}
	


	
}