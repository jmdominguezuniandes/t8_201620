package estructuras;


public class NodoHash<K,V> {

	private K llave;
	private V valor;
	private NodoHash next;
	
	public NodoHash(K llave, V valor) {
		super();
		this.llave = llave;
		this.valor = valor;
		next = null;
	}
	
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
	public V getValor() {
		return valor;
	}
	
	public void setValor(V valor) {
		this.valor = valor;
	}
	
	public void setNext(NodoHash pNodo){
		next = pNodo;
	}
	
	public NodoHash getNext(){
		return next;
	}
	
	
}
