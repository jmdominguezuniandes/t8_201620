package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Lista;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido sistema;

	
	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		sistema = new GrafoNoDirigido();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		Lista paraResponder = new Lista();
		Arco[] arcos = sistema.darArcosOrigen(identificador);
		for(int i = 0; i < arcos.length; i++){
			Arco temp = arcos[i];
			String ruta = (String) temp.darInformacion();
			if(contains(paraResponder, ruta) == false){
				paraResponder.add(ruta);
			}
		}
		
		Arco[] arcos1 = sistema.darArcosDestino(identificador);
		for(int i = 0; i < arcos1.length; i++){
			Arco temp = arcos1[i];
			String ruta = (String) temp.darInformacion();
			if(contains(paraResponder, ruta) == false){
				paraResponder.add(ruta);
			}
		}
		
		String rutas[] = new String[paraResponder.getSize()];
		for(int i = 0; i < paraResponder.getSize(); i++){
			rutas[i] = (String) paraResponder.get(i);
		}
		
		return rutas;
		
	}
	public boolean contains(Lista <String> l, String p){
		boolean respuesta = false;
		for(int i = 0; i < l.getSize(); i++){
			if(l.get(i).equals(p)){
				respuesta = true;
				break;
			}
		}
		return respuesta;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		//TODO Implementar
		Arco[] arcos = sistema.darArcos();
		Double distanciaMenor = Double.POSITIVE_INFINITY;
		for(int i = 0; i < arcos.length; i++){
			Arco temp = arcos[i];
			if(temp.darCosto() < distanciaMenor){
				distanciaMenor = temp.darCosto();
			}
		}
		return distanciaMenor;
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		Arco[] arcos = sistema.darArcos();
		Double distanciaMayor = 0.0 ;
		for(int i = 0; i < arcos.length; i++){
			Arco temp = arcos[i];
			if(temp.darCosto() > distanciaMayor){
				distanciaMayor = temp.darCosto();
			}
		}
		return distanciaMayor;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		String cadena;
        FileReader fr = new FileReader(RUTA_PARADEROS);
        BufferedReader br = new BufferedReader(fr);
        String numEstaciones = br.readLine();
        Integer num = Integer.valueOf(numEstaciones);
        sistema.setNumNodos(num);
        while((cadena = br.readLine())!=null) {
            String infoEstacion[] = cadena.split(";");
            String nombre = infoEstacion[0];
            String latitud = infoEstacion[1];
            Double lat = Double.valueOf(latitud);
            String longitud = infoEstacion[2];
            Double lon = Double.valueOf(longitud);
            Estacion aAgregar = new Estacion(nombre, lat, lon);
            sistema.agregarNodo(aAgregar);
        }
        br.close();
        fr.close();
		System.out.println("Se han cargado correctamente "+sistema.darNodos().length+" Estaciones");

		
		//TODO Implementar
		
		String ruta;
        fr = new FileReader(RUTA_RUTAS);
        br = new BufferedReader(fr);
        String numRutas = br.readLine();
        System.out.println(numRutas);
        while((cadena = br.readLine())!=null) {
          if(cadena.contains("---------------")){
        	  ruta = br.readLine();
        	  
        	  int numParadas = (Integer.valueOf(br.readLine()));
          
        	  String parada1[] = br.readLine().split(" ");
        	  for(int i = 0; i < (numParadas -1); i++){
        		  String estacion1 = parada1[0];
        		  String parada2[] = br.readLine().split(" ");
        		  String estacion2 = parada2[0];
        		  Double costo = Double.valueOf(parada2[1]);
        		  sistema.agregarArco(estacion1, estacion2, costo, ruta);
        		  parada1 = parada2;
        	  }
          }
          
          
        }
        br.close();
        fr.close();
		
		System.out.println("Se han cargado correctamente "+ sistema.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		Estacion respuesta = (Estacion) sistema.buscarNodo(identificador);
		return respuesta;
	}

}
